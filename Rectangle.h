#ifndef RECTANGLE_H
#define RECTANGLE_H

namespace MyObj
{
	class Rectangle
	{
	public:

		Rectangle(double x, double y, double width, double height);
		~Rectangle();

		const double getX();
		const double getY();
		const double getWidth();
		const double getHeight();

		void setX(double x);
		void setY(double y);
		void setWidth(double width);
		void setHeight(double height);

	private:

		double			m_x;
		double			m_y;
		double			m_width;
		double			m_height;
	};
}

#endif // #ifndef RECTANGLE_H
