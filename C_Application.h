#ifndef TEST_C_APPLICATION_H
#define TEST_C_APPLICATION_H

#include <vector>
#include <queue>
#include "Rectangle.h"
#include "Cannon.h"
#include "Clock.h"

static const float k_PI = 3.1415926536f;

class C_Application
{
public:
	
	typedef unsigned int T_PressedKey;

	C_Application(int screenWidth, int screenHeight);
	~C_Application();

	/// Tick is called on fix framerate (50fps)
	void Tick(T_PressedKey pressedKeys);
	void createNewClocks();

	static const T_PressedKey s_KeyLeft  = 0x01;
	static const T_PressedKey s_Control = 0x02;
	static const T_PressedKey s_KeyRight = 0x04;
	static const T_PressedKey s_KeySpace = 0x10;

private:

	//Dimensions of playable screen
	MyObj::Rectangle screen;
	
	// Members for sample tick
	
	Cannon*				m_Cannon;
	std::vector<Clock*>	m_Clocks;

	bool getOutOfBounds(Projectile* proj);
	void drawUI();

};

#endif // #ifndef TEST_C_APPLICATION_H
