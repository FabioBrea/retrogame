#include "Cannon.h"
#include "graphics.h"

static const float k_PI = 3.1415926536f;
static const double degree = 0.58;

/* Constructor */
Cannon::Cannon(double base_x, double base_y, double height, double width, double angle) :
	m_status(idle),
	m_base_x(base_x),
	m_base_y(base_y),
	m_height(height),
	m_width(width),
	m_angle(angle),
	m_rotation_speed(3 * degree),
	m_moving_speed(3),
	time_before_next_shoot(0),
	projectile_cooldown(20),
	cooldown(40),
	change_status_cooldown(0)
{

	double s = std::sin(m_angle);
	double c = std::cos(m_angle);

	m_top_x = m_base_x + m_height * s;
	m_top_y = m_base_y - m_height * c;

	/* The base of the cannon will be kept fixed */
	m_right_x = m_base_x + (m_width / 2) * c;
	m_right_y = m_base_y + (m_width / 2) * s;
	m_left_x = m_base_x - (m_width / 2) * c;
	m_left_y = m_base_y - (m_width / 2) * s;
}

/* Destructor */
Cannon::~Cannon()
{

}

/* Set the x coordinate of center of the base of the cannon */
void Cannon::setBaseX(double base_x)
{
	m_base_x = base_x;
}

/* Set the y coordinate of center of the base of the cannon */
void Cannon::setBaseY(double base_y)
{
	m_base_y = base_y;
}

/* Set the height of the cannon */
void Cannon::setHeight(double height)
{
	m_height = height;
}

/* Set the width of the cannon */
void Cannon::setWidth(double width)
{
	m_width = width;
}

/* Get the x coordinate of the center of the base of the cannon */
double Cannon::getBaseX()
{
	return m_base_x;
}

/* Get the y coordinate of the center of the base of the cannon */
double Cannon::getBaseY()
{
	return m_base_y;
}

/* Get the height of the cannon */
double Cannon::getHeight()
{
	return m_height;
}

/* Get the width of the cannon */
double Cannon::getWidth()
{
	return m_width;
}

/* Get the x coordinate of the shooting position of the cannon */
double Cannon::getTopX()
{
	return m_top_x;
}

/* Get the y coordinate of the shooting position of the cannon */
double Cannon::getTopY()
{
	return m_top_y;
}

/* Return the list of projectile shot */
std::vector<Projectile*>& Cannon::getShotProjectile()
{
	return shot_projectile;
}

/* These methods could be useful if the base of the cannon will be rotating */

/* Get the x coordinate of the right corner of the base of the cannon */
double Cannon::getRightX()
{
	return m_right_x;
}

/* Get the y coordinate of the right corner of the base of the cannon */
double Cannon::getRightY()
{
	return m_right_y;
}

/* Get the x coordinate of the left corner of the base of the cannon */
double Cannon::getLeftX()
{
	return m_left_x;
}

/* Get the y coordinate of the left corner of the base of the cannon */
double Cannon::getLeftY()
{
	return m_left_y;
}
/* End getters */

void Cannon::changeStatus()
{
	if (change_status_cooldown == 0)
	{
		if (m_status == idle)
		{
			m_status = moving;
		}
		else
		{
			m_status = idle;
		}
		change_status_cooldown += cooldown;
	}
}

/* Given an angle, it rotates the top point of the triangle */
void Cannon::move(bool right)
{
	this->clearWindow();

	if (m_status == idle)
	{
		/* Add angle, converted in radiant*/
		double rotation_sign = right ? 1 : -1;
		m_angle += rotation_sign * m_rotation_speed / 180 * k_PI;

		/* Stop the cannon from rotating more than 90� and -90�*/
		if (m_angle > k_PI / 2)
		{
			m_angle = k_PI / 2;
		}
		else if (m_angle < -k_PI / 2)
		{
			m_angle = -k_PI / 2;
		}

		double s = std::sin(m_angle);
		double c = std::cos(m_angle);

		double new_top_x = m_base_x + m_height * s;
		double new_top_y = m_base_y - m_height * c;
		m_right_x = m_base_x + (m_width / 2) * c;
		m_right_y = m_base_y + (m_width / 2) * s;
		m_left_x = m_base_x - (m_width / 2) * c;
		m_left_y = m_base_y - (m_width / 2) * s;
		m_top_x = new_top_x;
		m_top_y = new_top_y;
	}
	else
	{
		double moving_sign = right ? 1 : -1;
		m_base_x += moving_sign * m_moving_speed;
		m_right_x += moving_sign * m_moving_speed;
		m_left_x += moving_sign * m_moving_speed;
		m_top_x += moving_sign * m_moving_speed;
	}
}

/*
* Creates a new projectile in the current position, with the current angle
*/
void Cannon::shoot()
{
	if (time_before_next_shoot == 0)
	{
		/* Initializing projectile variables */
		double proj_width = 2;
		double proj_height = 2;
		double proj_top_left_x = m_top_x - proj_width / 2;
		double proj_top_left_y = m_top_y - proj_height;

		/* Create and push the projectile in the vector */
		shot_projectile.push_back(new Projectile(proj_top_left_x, proj_top_left_y, proj_height, proj_width, m_angle));
		time_before_next_shoot += projectile_cooldown;
	}
}

void Cannon::clearWindow()
{
	
	double left_most = getBaseX() - getWidth() - getHeight();
	double top_most = getBaseY() - getHeight();
	//double width_most = getHeight() * 2 + getRightX() - getLeftX();
	//double height_most = getHeight();
	double width_most = getHeight() * 3 + getRightX() - getLeftX();
	double height_most = getHeight() * 2;
	FillRect(static_cast<int>(left_most), static_cast<int>(top_most), static_cast<int>(width_most + 1), static_cast<int>(height_most + 1), GetRGB(0, 0, 0));
}

/* Decrease the cooldown for next projectile */
void Cannon::update()
{
	if (time_before_next_shoot > 0)
	{
		time_before_next_shoot--;
	}
	if (change_status_cooldown > 0)
	{
		change_status_cooldown--;
	}
	this->draw();
}

/*
* Clears the area of the cannon and draws it
*/
void Cannon::draw()
{
	/* Draw cannon */
	DrawLine(static_cast<int>(getTopX()), static_cast<int>(getTopY()), static_cast<int>(getLeftX()), static_cast<int>(getLeftY()), GetRGB(255, 0, 0), 2);
	DrawLine(static_cast<int>(getTopX()), static_cast<int>(getTopY()), static_cast<int>(getRightX()), static_cast<int>(getRightY()), GetRGB(0, 255, 0), 2);
	DrawLine(static_cast<int>(getLeftX()), static_cast<int>(getLeftY()), static_cast<int>(getRightX()), static_cast<int>(getRightY()), GetRGB(0, 0, 255), 2);
}
