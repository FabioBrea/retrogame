#ifndef CANNON_H
#define CANNON_H

#include <algorithm>
#include <vector>
#include "Projectile.h"

enum status {idle, moving};

class Cannon
{
public:

	Cannon(double base_x, double base_y, double height, double width, double angle);
	~Cannon();

	void update();
	void changeStatus();
	void move(bool right);

	double getBaseX();
	double getBaseY();
	double getHeight();
	double getWidth();
	double getTopX();
	double getTopY();

	void setBaseX(double base_x);
	void setBaseY(double base_y);
	void setHeight(double height);
	void setWidth(double width);

	void draw();

	void shoot();
	void clearWindow();

	std::vector<Projectile*>& getShotProjectile();

private:

	status			m_status;
	double			change_status_cooldown;
	double			cooldown;
	
	//Fixed rotation point
	double			m_base_x;
	double			m_base_y;
	//Fixed dimension of triangle
	double			m_height;
	double			m_width;

	double			m_angle;
	int				time_before_next_shoot;
	int				projectile_cooldown;

	//Rotating points
	double			m_top_x;
	double			m_top_y;
	double			m_left_x;
	double			m_left_y;
	double			m_right_x;
	double			m_right_y;

	double			m_rotation_speed;
	double			m_moving_speed;

	double getRightY();
	double getRightX();
	double getLeftX();
	double getLeftY();

	std::vector<Projectile*> shot_projectile;

};

#endif // #ifndef CANNON_H
