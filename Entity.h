#pragma once
#ifndef ENTITY_H
#define ENTITY_H

#include "Rectangle.h"

namespace MyObj
{
	class Entity : public Rectangle
	{
	public:

		Entity(double x, double y, double width, double height, Rectangle world, double vx = 0.0, double vy = 0.0);
		~Entity();

		virtual void			update() = 0;
		virtual void			draw() = 0;
		virtual void			clearWindow() = 0;

		double					getVx();
		double					getVy();
		Rectangle				getWorld();

		void					setVx(double vx);
		void					setVy(double vy);
		void					setWorld(Rectangle world);

		template <typename T> bool isCollisionDetected(T* obj);

	private:

		double					m_vx;
		double					m_vy;
		Rectangle				m_world;
	};

	/* Check for collisions with another entity, if there are return true */
	template <typename T> bool Entity::isCollisionDetected(T* obj)
	{
		/* Check if the bound box are outside*/
		if (getX() + getWidth() < obj->getX() ||
			getY() + getHeight() < obj->getY() ||
			getX() > obj->getX() + obj->getWidth() ||
			getY() > obj->getY() + obj->getHeight())
		{
			return false;
		}

		return true;
	}
}
#endif // #ifndef ENTITY_H