#include "Clock.h"
#include "graphics.h"
#include "time.h"

static const float k_PI = 3.1415926536f;

Clock::Clock(double x, double y, double width, double height, MyObj::Rectangle world, double vx, double vy) :
	MyObj::Entity(x, y, width, height, world, vx, vy),
	hours(0),
	minutes(0),
	seconds(0)
{
	x_center = getX() + getWidth() / 2;
	y_center = getY() + getHeight() / 2;
}

Clock::~Clock()
{
	clearWindow();
}


void Clock::draw()
{
	//Draws square
	DrawLine(static_cast<int>(getX()), static_cast<int>(getY()), static_cast<int>(getX() + getWidth()), static_cast<int>(getY()), GetRGB(255, 255, 255), 2);
	DrawLine(static_cast<int>(getX()), static_cast<int>(getY()), static_cast<int>(getX()), static_cast<int>(getY() + getHeight()), GetRGB(255, 255, 255), 2);
	DrawLine(static_cast<int>(getX() + getWidth()), static_cast<int>(getY()), static_cast<int>(getX() + getWidth()), static_cast<int>(getY() + getHeight()), GetRGB(255, 255, 255), 2);
	DrawLine(static_cast<int>(getX()), static_cast<int>(getY() + getHeight()), static_cast<int>(getX() + getWidth()), static_cast<int>(getY() + getHeight()), GetRGB(255, 255, 255), 2);

	//Draws hours
	double hours_angle = hours * (2 * k_PI / 12);
	double sin_hours = std::sin(hours_angle);
	double cos_hours = std::cos(hours_angle);
	double x_hour = x_center + (getWidth() / 4) * sin_hours;
	double y_hour = y_center - (getHeight() / 4) * cos_hours;
	DrawLine(static_cast<int>(x_center), static_cast<int>(y_center), static_cast<int>(x_hour), static_cast<int>(y_hour), GetRGB(255, 0, 0));

	//Draws minutes
	double minutes_angle = minutes * (2 * k_PI / 60);
	double sin_minutes = std::sin(minutes_angle);
	double cos_minutes = std::cos(minutes_angle);
	double x_minutes = x_center + (getWidth() / 2) * sin_minutes;
	double y_minutes = y_center - (getHeight() / 2) * cos_minutes;
	DrawLine(static_cast<int>(x_center), static_cast<int>(y_center), static_cast<int>(x_minutes), static_cast<int>(y_minutes), GetRGB(0, 255, 0));

	//Draws seconds
	double seconds_angle = seconds * (2 * k_PI / 60);
	double sin_seconds = std::sin(seconds_angle);
	double cos_seconds = std::cos(seconds_angle);
	double x_seconds = x_center + (getWidth() / 2) * sin_seconds;
	double y_seconds = y_center - (getHeight() / 2) * cos_seconds;
	DrawLine(static_cast<int>(x_center), static_cast<int>(y_center), static_cast<int>(x_seconds), static_cast<int>(y_seconds), GetRGB(0, 0, 255));

}

void Clock::clearWindow()
{
	FillRect(static_cast<int>(getX() - 1), static_cast<int>(getY() - 1), static_cast<int>(getWidth() + 3), static_cast<int>(getHeight() + 3), GetRGB(0, 0, 0));
}

/* Update the current time and position. If it touches the boundaries, bounce back */
void Clock::update()
{
	this->clearWindow();

	GetTime(hours, minutes, seconds);
	
	setX(getX() + getVx());
	setY(getY() - getVy());

	MyObj::Rectangle* world = &getWorld();

	//Bounce if it touches the boundaries
	if (getX() < world->getX())
	{
		setX(getWorld().getX());
		setVx(getVx() * -1);
	}
	else if (getX() + getWidth() > world->getX() + world->getWidth())
	{
		setX(world->getX() + world->getWidth() - getWidth());
		setVx(getVx() * -1);
	}

	if (getY() < world->getY())
	{
		setY(world->getY());
		setVy(getVy() * -1);
	}
	else if (getY() + getHeight() > world->getY() + world->getHeight())
	{
		setY(world->getY() + world->getHeight() - getHeight());
		setVy(getVy() * -1);
	}

	x_center = getX() + getWidth() / 2;
	y_center = getY() + getHeight() / 2;

	this->draw();
}

