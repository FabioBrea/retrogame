#include "Rectangle.h"

namespace MyObj
{
	/* Constructor */
	Rectangle::Rectangle(double x, double y, double width, double height) :
		m_x(x),
		m_y(y),
		m_width(width),
		m_height(height)
	{

	}

	/* Destructor */
	Rectangle::~Rectangle()
	{

	}

	/* Get the top left point abscissa */
	const double Rectangle::getX()
	{
		return m_x;
	}

	/* Get the top left point ordinate */
	const double Rectangle::getY()
	{
		return m_y;
	}

	/* Get the width of the rectangle */
	const double Rectangle::getWidth()
	{
		return m_width;
	}

	/* Get the height of the rectangle */
	const double Rectangle::getHeight()
	{
		return m_height;
	}

	/* Set the top left point abscissa */
	void Rectangle::setX(double x)
	{
		m_x = x;
	}

	/* Set the top left point ordinate */
	void Rectangle::setY(double y)
	{
		m_y = y;
	}

	/* Set the width of the rectangle */
	void Rectangle::setWidth(double width)
	{
		m_width = width;
	}

	/* Set the height of the rectangle */
	void Rectangle::setHeight(double height)
	{
		m_height = height;
	}

}