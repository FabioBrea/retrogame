#include "C_Application.h"
#include "graphics.h"
#include "time.h"
#include <ctime>

int high_score = 0;

C_Application::C_Application(int screenWidth, int screenHeight) :
	screen(0, 0.1 * screenHeight, screenWidth - 0, 0.9 * screenHeight)

{ 
	m_Cannon = new Cannon((screen.getX() + screen.getWidth()) / 2, screen.getY() + screen.getHeight() - 25, 30, 10, 0);
}

double fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

void C_Application::createNewClocks()
{
	double clock_width = 100;
	double clock_height = 100;
	srand((unsigned)time(0));

	double randomX = fRand(screen.getX(), screen.getX() + screen.getWidth());
	double randomY = fRand(screen.getY(), screen.getY() + screen.getHeight());
	double randomAngle = fRand(-180, 180) / 180 * k_PI;
	double randomSecondAngle = fRand(-180, 180) / 180 * k_PI;
	double randomSpeed = fRand(1, 5);
	double randomSecondSpeed = fRand(1, 5);
	double randomXSecond = fRand(screen.getX(), screen.getX() + screen.getWidth());
	double randomYSecond = fRand(screen.getY(), screen.getY() + screen.getHeight());

	while (!(randomXSecond > randomX + clock_width || randomXSecond < randomX || randomYSecond > randomY + clock_height || randomYSecond < randomY))
	{
		randomXSecond = fRand(screen.getX(), screen.getX() + screen.getWidth());
		randomYSecond = fRand(screen.getY(), screen.getY() + screen.getHeight());
	}
	m_Clocks.push_back(new Clock(randomX, randomY, clock_width, clock_height, screen, randomSpeed * std::sin(randomAngle), randomSpeed * std::cos(randomAngle)));
	m_Clocks.push_back(new Clock(randomXSecond, randomYSecond, clock_width, clock_height, screen, randomSecondSpeed * std::sin(randomSecondAngle), randomSecondSpeed * std::cos(randomSecondAngle)));
}


C_Application::~C_Application()
{

}

void C_Application::Tick(T_PressedKey pressedKeys)
{
	//If there are no clocks, add them
	if (m_Clocks.size() == 0)
	{
		createNewClocks();
	}

	// Key processing
	if (pressedKeys & s_Control)
	{
		m_Cannon->changeStatus();
	}

	if (pressedKeys & s_KeyLeft)
	{
		m_Cannon->move(false);
	}

	if (pressedKeys & s_KeyRight)
	{
		m_Cannon->move(true);
	}

	if (pressedKeys & s_KeySpace)
	{
		m_Cannon->shoot();
	}

	// Draws cannon and clocks
	m_Cannon->update();
	for (Clock* clock : m_Clocks)
	{
		clock->update();
	}

	// This vector will contain elements if big clocks are shattered
	std::vector<Clock*> new_clocks;

	std::vector<Projectile*>& shotted_proj = m_Cannon->getShotProjectile();
	std::vector<Projectile*>::const_iterator proj_it = shotted_proj.begin();

	std::vector<Clock*>::const_iterator first_it = m_Clocks.begin();
	while (first_it != m_Clocks.end())
	{
		std::vector<Clock*>::const_iterator second_it = first_it + 1;
		while (second_it != m_Clocks.end())
		{
			bool collision = (*first_it)->isCollisionDetected(*second_it);

			if (collision)
			{
				double first_left_x = (*first_it)->getX();
				double first_left_y = (*first_it)->getY();
				double first_width = (*first_it)->getWidth();
				double first_height = (*first_it)->getHeight();
				double second_left_x = (*second_it)->getX();
				double second_left_y = (*second_it)->getY();
				double second_width = (*second_it)->getWidth();
				double second_height = (*second_it)->getHeight();
				if (((first_left_x <= second_left_x + second_width) || (second_left_x <= first_left_x + first_width)))
				{
					(*first_it)->setVx(-1 * (*first_it)->getVx());
					(*second_it)->setVx(-1 * (*second_it)->getVx());
				}
				
				if ((first_left_y <= second_left_y + second_height) || (second_left_y <= first_left_y + first_height))
				{
					(*first_it)->setVy(-1 * (*first_it)->getVy());
					(*second_it)->setVy(-1 * (*second_it)->getVy());
				}
				
			}
			++second_it;
		}
		++first_it;
	}

	while (proj_it != shotted_proj.end())
	{
		(*proj_it)->update();

		if (getOutOfBounds(*proj_it))
		{
			(*proj_it)->clearWindow();
			proj_it = shotted_proj.erase(proj_it);
			continue;
		}

		/* Check for collision between the projectile and all clocks */
		bool proj_hit = false;
		std::vector<Clock*>::const_iterator clock_it = m_Clocks.begin();
		while (clock_it != m_Clocks.end())
		{
			bool clock_hit = false;
			/* Check for collisions */
			if ((*proj_it)->isCollisionDetected(*clock_it))
			{
				high_score++;
				proj_hit = true;
				clock_hit = true;
			}

			if (clock_hit)
			{
				(*clock_it)->clearWindow();
				(*proj_it)->clearWindow();
				//Add two clocks in the queue, if bigger clocks are hit
				if ((*clock_it)->getWidth() > 5 && (*clock_it)->getHeight() > 5)
				{
					double vy = (*clock_it)->getVx();
					double vx = (*clock_it)->getVy();
					new_clocks.push_back(new Clock((*clock_it)->getX(), (*clock_it)->getY(), (*clock_it)->getWidth() / 2, (*clock_it)->getHeight() / 2, screen, vx, vy));
					new_clocks.push_back(new Clock((*clock_it)->getX() + (*clock_it)->getWidth() / 2 + 1, (*clock_it)->getY(), (*clock_it)->getWidth() / 2, (*clock_it)->getHeight() / 2, screen, -vx, -vy));
				}
				clock_it = m_Clocks.erase(clock_it);
			}
			else
			{
				++clock_it;
			}
		}

		/* Destroy the projectile if it hit a clock*/
		if (proj_hit)
		{
			proj_it = shotted_proj.erase(proj_it);
		}
		else
		{
			++proj_it;
		}
	}

	// Add new clocks to the current clocks
	std::vector<Clock*>::const_iterator clock_it = new_clocks.begin();
	while (clock_it != new_clocks.end())
	{
		m_Clocks.push_back(*(clock_it));
		++clock_it;
	}

	m_Cannon->draw();
	drawUI();
}

void C_Application::drawUI()
{
	DrawLine(static_cast<int>(screen.getX()), static_cast<int>(screen.getY()), static_cast<int>(screen.getX() + screen.getWidth()), static_cast<int>(screen.getY()), RGB(255, 255, 255));
	FillRect(10, 10, 20, 15, GetRGB(0, 0, 0));
	PrintText(std::to_string(high_score), 10, 10);

}

/* Check if the projectile is outside the window */
bool C_Application::getOutOfBounds(Projectile* proj)
{
	double top_left_x = proj->getLeftTopX();
	double top_left_y = proj->getLeftTopY();
	double height = proj->getHeight();
	double width = proj->getWidth();

	/* Check if the bound box are outside*/
	if (top_left_x + width < screen.getX() ||
		top_left_y + height < screen.getY() ||
		top_left_x > screen.getX() + screen.getWidth() ||
		top_left_y > screen.getY() + screen.getHeight())
	{
		return true;
	}

	return false;
}