#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <algorithm>
#include "Clock.h"

class Projectile
{
public:

	Projectile(double left_top_x, double left_top_y, double height, double width, double shooting_angle, double speed = 5);
	~Projectile();

	void update();

	bool isCollisionDetected(Clock* clock);
	void clearWindow();

	double getLeftTopX();
	double getLeftTopY();
	double getWidth();
	double getHeight();

private:

	//Fixed points to draw
	double			m_left_top_x;
	double			m_left_top_y;
	double			m_height;
	double			m_width;

	double			m_vx;
	double			m_vy;

	void draw();
};

#endif // #ifndef PROJECTILE_H
