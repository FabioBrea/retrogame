#include "Entity.h"

namespace MyObj
{
	/* Constructor */
	Entity::Entity(double x, double y, double width, double height, Rectangle world, double vx, double vy) :
		Rectangle(x, y, width, height),
		m_vx(vx),
		m_vy(vy),
		m_world(world)
	{

	}

	/* Destructor */
	Entity::~Entity()
	{

	}

	/* Return the x velocity of the entity */
	double Entity::getVx()
	{
		return m_vx;
	}

	/* Return the y velocity of the entity */
	double Entity::getVy()
	{
		return m_vy;
	}

	/* Return the rectangle representing the screen */
	Rectangle Entity::getWorld()
	{
		return m_world;
	}

	/* Set the x velocity of the entity */
	void Entity::setVx(double vx)
	{
		m_vx = vx;
	}

	/* Set the y velocity of the entity */
	void Entity::setVy(double vy)
	{
		m_vy = vy;
	}

	/* Set the rectangle representing the screen */
	void Entity::setWorld(Rectangle world)
	{
		m_world = world;
	}
}