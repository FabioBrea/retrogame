#ifndef CLOCK_H
#define CLOCK_H

#include "Entity.h"
#include <algorithm>
#include <vector>

class Clock : public MyObj::Entity
{
public:

	Clock(double x, double y, double width, double height, MyObj::Rectangle world, double vx = 0.0, double vy = 0.0);
	~Clock();

	/* Inherited members */
	void update();
	void draw();
	void clearWindow();

private:

	//Fixed points to draw
	double			x_center;
	double			y_center;

	int				hours;
	int				minutes;
	int				seconds;
};



#endif // #ifndef CLOCK_H
