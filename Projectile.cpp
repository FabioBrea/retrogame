#include "Projectile.h"
#include "graphics.h"

static const float k_PI = 3.1415926536f;

/* Constructor */
Projectile::Projectile(double left_top_x, double left_top_y, double height, double width, double shooting_angle, double speed) :
	m_left_top_x(left_top_x),
	m_left_top_y(left_top_y),
	m_height(height),
	m_width(width),
	m_vx(speed * std::sin(shooting_angle)),
	m_vy(speed * std::cos(shooting_angle))
{

}

/* Destructor */
Projectile::~Projectile()
{

}

/* Moves the projectile and draws it*/
void Projectile::update()
{
	this->clearWindow();
	m_left_top_x = m_left_top_x + m_vx;
	m_left_top_y = m_left_top_y - m_vy;
	this->draw();
}

/* Get the x coordinate of top left corner */
double Projectile::getLeftTopX()
{
	return m_left_top_x;
}

/* Get the y coordinate of top left corner */
double Projectile::getLeftTopY()
{
	return m_left_top_y;
}

/* Get the width of the projectile */
double Projectile::getWidth()
{
	return m_width;
}

/* Get the height of the projectile */
double Projectile::getHeight()
{
	return m_height;
}

/* Check for a collision with a clock, return true if there is a collision */
bool Projectile::isCollisionDetected(Clock* clock)
{
	/* Check if the bound box are outside*/
	if (m_left_top_x + m_width < clock->getX() ||
		m_left_top_y + m_height < clock->getY() ||
		m_left_top_x > clock->getX() + clock->getWidth() ||
		m_left_top_y > clock->getY() + clock->getHeight())
	{
		return false;
	}

	return true;
}

void Projectile::clearWindow()
{
	FillRect(static_cast<int>(getLeftTopX()), static_cast<int>(getLeftTopY()), static_cast<int>(getWidth()) + 1, static_cast<int>(getHeight()) + 1, GetRGB(0, 0, 0));
}

/* Draws the projectile */
void Projectile::draw()
{
	DrawLine(static_cast<int>(m_left_top_x), static_cast<int>(m_left_top_y), static_cast<int>(m_left_top_x), static_cast<int>(m_left_top_y + m_height), GetRGB(255, 0, 0));
	DrawLine(static_cast<int>(m_left_top_x), static_cast<int>(m_left_top_y), static_cast<int>(m_left_top_x + m_width), static_cast<int>(m_left_top_y), GetRGB(255, 0, 0));
	DrawLine(static_cast<int>(m_left_top_x + m_width), static_cast<int>(m_left_top_y), static_cast<int>(m_left_top_x + m_width), static_cast<int>(m_left_top_y + m_height), GetRGB(255, 0, 0));
	DrawLine(static_cast<int>(m_left_top_x), static_cast<int>(m_left_top_y + m_height), static_cast<int>(m_left_top_x + m_width), static_cast<int>(m_left_top_y + m_height), GetRGB(255, 0, 0));
}
